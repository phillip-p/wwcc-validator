const { ipcRenderer, dialog } = require('electron').remote;
const ipc = ipcRenderer;
const fs = require('fs');
const path = require('path');
const XLSX = require('xlsx');
const CSV = require('csvtojson');
const chrome = require('selenium-webdriver/chrome'); 
const driverPath = require('chromedriver').path;
console.log(driverPath);

const webdriver = require('selenium-webdriver'),
	By = webdriver.By,
	until = webdriver.until;

const SUCCESS = 0
const WARNING = 1
const ERROR = 2
const CLEAR = 3

const usr = document.getElementById("username");
const pwd = document.getElementById("password");
const delay = document.getElementById("delay")
const checkbox = document.getElementById("remember");
const fileBtn = document.getElementById("fileBtn");
const delayBtn = document.getElementById("delayBtn");
const startBtn = document.getElementById("startBtn");
const delayContent = document.getElementById("delay-content");
const errorContent = document.getElementById("error-content");

let delayVal = 0.5;
let saveData = false;
let employees = null;
let driver;

let loadData = () => {
	let p = path.join(__dirname, 'save.dat');

	fs.readFile(p, 'utf8', (err, data) => {
		if (err) return console.log(err);
		
		data = JSON.parse(data);
		saveData = data["checked"] === "true";
		
		if (saveData) {
			checkbox.checked = true;
			usr.value = data["usr"];
			pwd.value = data["pwd"];
		}
	});
}


let save = () => {
	let p = path.join(__dirname, 'save.dat');

	saveData = checkbox.checked;
	content = `{"checked": "${saveData}", "usr": "${saveData ? usr.value : ""}", "pwd": "${saveData ? pwd.value : ""}"}`
	fs.writeFile(p, content, (err) => {
		if (err) console.log(err);
	});
}


let showMsg = (err_type, msg) => {
	switch (err_type) {
		case SUCCESS:
			errorContent.style.color = "#59FF59";
			break;
		case WARNING:
			errorContent.style.color = "#FFD600";
			break;
		case ERROR:
			errorContent.style.color = "#FF5959";
			break;
	}
	errorContent.textContent = msg;
	return err_type;
};


let setDelay = () => {
	if (delay.value === undefined || delay.value === null || delay.value === "") {
		return showMsg(ERROR, "Error: no value entered");
	}

	parsedVal = parseFloat(delay.value);
	delay.value = "";

	if (isNaN(parsedVal)) {
		return showMsg(ERROR, "Error: delay must be a number");
	}

	if (parsedVal < 0) {
		return showMsg(ERROR, "Error: delay can't be negative");
	} else if (parsedVal < 10e-5) {
		showMsg(WARNING, "Warning: short delay")
	} else if (parsedVal > 30) {
		return showMsg(ERROR, "Error: delay too long");
	} else if (parsedVal > 2) {
		showMsg(WARNING, "Warning: long delay");
	} else {
		showMsg(SUCCESS, "Delay has been changed");
	}

	delayVal = parsedVal;

	delayContent.textContent = `Delay set to ${delayVal} sec`;
}


let staffHandler = (staff) => {
	employees = {};

	let count = 0;
	for (const i in staff) {
		let dob = staff[i]["Dob"]
		if (dob === undefined || dob === "") continue;

		wwc = staff[i]["WWC NSW #"];
		if (wwc === undefined || wwc === "") continue;

		employees[count] = {};

		employees[count]["WWCC"] = wwc;

		dob = dob.split("-");
		employees[count]["DOB-Y"] = dob[0];
		employees[count]["DOB-M"] = dob[1];
		employees[count]["DOB-D"] = dob[2];
		employees[count]["DOB"] = `${dob[2]}/${dob[1]}/${dob[0]}`;

		employees[count]["Last"] = staff[i]["Last Name"];

		count++;
	}
}


let fileHandler = (file) => {
	let csvIndex = file.lastIndexOf(".csv") + 4;
	let tsvIndex = file.lastIndexOf(".tsv") + 4;

	if (csvIndex === file.length) {
		CSV()
			.fromFile(file)
			.then((staff) => {
				staffHandler(staff);
				showMsg(SUCCESS, "File loaded");
			});
	} else if (tsvIndex === file.length) {
		return showMsg(ERROR, "not implemented");
	} else {
		let workbook = XLSX.readFile(file, {
			type: 'binary',
			sheetStubs: true,
			raw: false,
			cellText: false,
			cellDates: true,
		});

		let sheet = workbook.Sheets[workbook.SheetNames[0]];
		let staff = XLSX.utils.sheet_to_json(sheet, {raw:false,dateNF:'yyyy-mm-dd'});
		staffHandler(staff);
		showMsg(SUCCESS, "File loaded");
	}
}


delayBtn.onclick = () => setDelay();


fileBtn.onclick = () => {
	showMsg(WARNING, "Loading file...");
	dialog.showOpenDialog({ 
		properties: ['openFile'],
		filters: [
			{ name: 'Spreadsheets', extensions: ['xlsx', 'csv', 'tsv'] },
		],
	}).then(result => {
		file = result.filePaths[0];
		if (file == undefined) {
			return showMsg(ERROR, "Error: no file selected");
		}
		fileHandler(file);
	});

};


let sleep = (sec) => {
	randomTime = 1000 * Math.random() * sec / 2;
	return new Promise(resolve => setTimeout(resolve, randomTime));
};


let login = async () => {
	driver.findElement(By.id("Username"))
		.then(e => {
			e.sendKeys(usr.value);
		});

	await sleep(delayVal);

	driver.findElement(By.id("Password"))
		.then(e => {
			e.sendKeys(pwd.value);
		});

	await sleep(delayVal);

	driver.findElement(By.id("Login")).click();

	await sleep(delayVal);
}


let enter_staff = async () => {
	let total = Object.keys(employees).length;

	await driver.wait(until.titleIs('NSW ONLY Working with Children - Verify Employees & Volunteer WWC'), 10000);

	showMsg(WARNING, "Adding staff...");

	let current = 1;
	for (const i in employees) {
		let fam = `Criteria_${i}__FamilyName`;
		let dob = `Criteria_${i}__BirthDate`;
		let wwc = `Criteria_${i}__AuthorisationNumber`;

		await driver.findElement(By.id(fam)).sendKeys(employees[i]["Last"]);
		await sleep(delayVal);
		
		await driver.findElement(By.id(dob)).sendKeys(employees[i]["DOB"]);
		await sleep(delayVal);
		
		await driver.findElement(By.id(wwc)).sendKeys(employees[i]["WWCC"]);
		await sleep(delayVal);

		if (i < total - 1) {
			current++;
			await driver.findElement(By.id("Add")).click();
		}

		await sleep(2 * delayVal);
	}

	if (current == total) {
		showMsg(SUCCESS, "Staff added");
	}
}


startBtn.onclick = async () => {
	if (usr.value === undefined || usr.value === "") {
		return showMsg(ERROR, "Error: no username entered");
	} else if (pwd.value === undefined || pwd.value === "") {
		return showMsg(ERROR, "Error: no password entered");
	} else if (employees === null) {
		return showMsg(ERROR, "Error: no file selected");
	} else if (Object.keys(employees).length < 1) {
		return showMsg(ERROR, "Error: no staff to check");
	} else {
		showMsg(WARNING, "Logging in..");
	}

	let service = new chrome.ServiceBuilder(driverPath).build();
	chrome.setDefaultService(service);
	
	driver = new webdriver.Builder()
		.withCapabilities(webdriver.Capabilities.chrome())
		.build();

	driver.get("https://wwccheck.ccyp.nsw.gov.au/Employers/Login");

	await login();

	showMsg(WARNING, "Adding staff...");

	await enter_staff();
};


loadData();
errorContent.innerText = "\n";

setInterval(save, 1000);